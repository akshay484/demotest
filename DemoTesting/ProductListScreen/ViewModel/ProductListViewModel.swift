//
//  File.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 24/09/21.
//

import Foundation
import ObjectMapper

//MARK: -  HomeViewModel delegate to get success/failure from API

protocol ProductListViewModelDelegate {
    func didFetchProductListData(responseData: Any)
    func didFailFetchProductListData(error: NSError)
    
}

class ProductListViewModel:Networking,MainServiceDelegate{
    
    
    var productListViewModelDelegate: ProductListViewModelDelegate?
    
    
    
    //MARK: - if we get success from the Networking class methods then this method is called.
    func didFetchData(responseData: Any) {
            self.productListViewModelDelegate?.didFetchProductListData(responseData: responseData)
    }
    
    //MARK: - if we get failure from the Networking class methods then this method is called.
    func didFailWithError(error: NSError) {
            self.productListViewModelDelegate?.didFailFetchProductListData(error: error)
    }
    
    
    //MARK:-  Service call for getting product list
    func getHomeDataFromServer(delegate:ProductListViewModelDelegate) -> Void {
        self.productListViewModelDelegate = delegate
        super.mainServerdelegate = self
        let url = API.productListURL
        super.getCallWithAlamofire(serverUrl: url)
    }
    
    //MARK:-  Parsing product data from JSON
    func parseProductListData(completion: @escaping ((Bool , String , String , ProductListModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<ProductListModel>().map(JSONObject: responseDict)
            if(jsonData?.isSuccess == true){
                completion(true , "" , ErrorMessages.APIAlertTitle,jsonData)
            }else{
                completion(false , jsonData?.message ?? "", ErrorMessages.APIAlertTitle,nil)
            }
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
    
    
    
    
}
