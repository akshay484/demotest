//
//  ProductListView.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 24/09/21.
//

import UIKit

class ProductListView: AppMainVc {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var tblProductList: UITableView!
    @IBOutlet weak var viewCartTotal: UIView!
    @IBOutlet weak var lblTotalAmt: UILabel!
    @IBOutlet weak var lblTotalAmtValue: UILabel!
    @IBOutlet weak var viewCartCount: UIView!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var stackBottomView: UIStackView!
    @IBOutlet weak var heightStackBottom: NSLayoutConstraint!
    
    
    //MARK: - product variant view
    @IBOutlet weak var viewProductVariant: UIView!
    @IBOutlet weak var tblProductVariant: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAddItem: UIButton!
    @IBOutlet weak var stackVariantBottomView: UIStackView!
    @IBOutlet weak var heightStackVariantBottom: NSLayoutConstraint!
    @IBOutlet weak var heightVariantTable: NSLayoutConstraint!
    @IBOutlet weak var lblItemTotal: UILabel!
    @IBOutlet weak var viewParentVariant: UIView!
    var slideUpViewHeight = 200
    var selectedVariantRow: Int?
    
    //MARK:- previous customizationView outlets
    @IBOutlet weak var lblPrevCustTitle: UILabel!
    @IBOutlet weak var lblPrevCustItemTitle: UILabel!
    @IBOutlet weak var lblPrevCustItemVariantTitle: UILabel!
    @IBOutlet weak var btnRepeatItem: UIButton!
    @IBOutlet weak var btnChooseItem: UIButton!
    @IBOutlet weak var viewPrevCustParent: UIView!
    @IBOutlet weak var viewPrevCustChild: UIView!
    
    var totalCartAmount:Float?{
        didSet{
            self.lblTotalAmtValue.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: "₹ \(self.totalCartAmount ?? 0.0)")
        }
    }
    
    //MARK: - view model object to make API calls and parsing
    lazy var productListViewModel: ProductListViewModel = ProductListViewModel()
    
    //MARK: - to display list of products
    var productListModel: [ProductDataModel]? = [ProductDataModel](){
        didSet{
            DispatchQueue.main.async {
                self.tblProductList.reloadData()
                self.lblNavTitle.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.whiteColor, fontSize: 18, text: self.productListModel?.first?.subCatName ?? "")
            }
        }
    }
    
    
    //MARK: - to display list of products in cart
    var cartProducts: [ProductDataModel]? = [ProductDataModel](){
        didSet{
            
            if let cartProducts = self.cartProducts{
                
                DispatchQueue.main.async {
                    self.tblProductVariant.reloadData()
                    self.lblTitle.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.whiteColor, fontSize: 18, text: self.cartProducts?.last?.prdName ?? "")
                }
                
                
                self.viewParentVariant.isHidden = false
                self.viewProductVariant.isHidden = false
                self.heightStackVariantBottom.constant = 48
                
                //MARK:- show proceed view
                self.stackBottomView.isHidden = false
                self.heightStackBottom.constant = 64
                
                self.lblCartCount.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 9, text: "\(cartProducts.count)")
                
                
            }
        }
    }
    
    //MARK: - to display list of variants for a product
    var variantData: [ProductVariantListData] = [ProductVariantListData](){
        didSet{
            DispatchQueue.main.async {
                self.heightVariantTable.constant = CGFloat(44 * self.variantData.count)
                self.tblProductVariant.reloadData()
            }
        }
    }
    
    //MARK: - to keep track of selected products alongwith their variants and quantities of their variants
    var selectedProduct: ProductDataModel?
    
    var itemTotalPrice = 0
    
    
    var selectedCartData: [CartModel] = [CartModel](){
        didSet{
            
            let prodQtyCount = selectedCartData.filter { cartObj in
                (cartObj.productModel?.prdId ?? "") == (self.selectedProduct?.prdId ?? "")
            }.count
            
            self.productListModel?.filter({ prdObj in
                (prdObj.prdId ?? "") == (self.selectedProduct?.prdId ?? "")
            }).first?.qty = prodQtyCount
            
            
            for objProd in self.selectedCartData{
                let individualPrice = Float(objProd.variantModel?.prdVarPrice ?? "0") ?? 0.0
                self.totalCartAmount = individualPrice + (self.totalCartAmount ?? 0.0)
            }
            
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUISettings()
        self.tblProductList.delegate = self
        self.tblProductList.dataSource = self
        self.tblProductVariant.delegate = self
        self.tblProductVariant.dataSource = self
        productListViewModel.getHomeDataFromServer(delegate: self)
    }
    
    
    //MARK: - UI settings
    func setupUISettings(){
        self.stackBottomView.isHidden = true
        self.heightStackBottom.constant = 0
        
        self.navView.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        
        self.btnProceed.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.btnProceed.setTitle("PROCEED", for: .normal)
        self.btnProceed.setTitleColor( UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
        
        
        self.btnRepeatItem.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.btnRepeatItem.setTitle("REPEAT", for: .normal)
        self.btnRepeatItem.setTitleColor( UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
        
        
        self.btnChooseItem.backgroundColor = UIColor.init(hexString: ColorCodes.whiteColor)
        self.btnChooseItem.setTitle("I'll choose", for: .normal)
        self.btnChooseItem.setTitleColor( UIColor.init(hexString: ColorCodes.mainThemeColor), for: .normal)
        
        
        self.lblTotalAmt.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: "Total Amount")
        self.lblCartCount.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 9, text: "0")
        self.viewCartTotal.backgroundColor = UIColor.init(hexString: ColorCodes.lightGreyColor)
        self.viewCartCount.backgroundColor = UIColor.init(hexString: ColorCodes.darkGreyColor)
        self.viewCartCount.layer.cornerRadius = self.viewCartCount.frame.width/2
        
        
        //MARK: - for bottom variant view
        self.viewProductVariant.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.btnAddItem.setTitle("ADD ITEM", for: .normal)
        self.btnAddItem.setTitleColor(UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
        self.btnAddItem.layer.cornerRadius = 8
        self.btnAddItem.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.heightStackVariantBottom.constant = 0
    }
    
    
}


//MARK: - tableview delegate and data source methods
extension ProductListView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblProductList{ //MARK: - if tableview is product list
            return self.productListModel?.count ?? 0
        }else if tableView == self.tblProductVariant{ //MARK: - if tableview is variant list
            return self.variantData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblProductList{ //MARK: - if tableview is product list
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.productCell, for: indexPath) as? ProductCell{
                
                cell.productDataModel = self.productListModel?[indexPath.row]
                
                cell.btnAdd.tag = indexPath.row
                cell.btnPlus.tag = indexPath.row
                cell.btnMinus.tag = indexPath.row
                
                cell.btnAdd.addTarget(self, action: #selector(self.btnAddPressed), for: .touchUpInside)
                cell.btnPlus.addTarget(self, action: #selector(self.btnPlusPressed(_:)), for: .touchUpInside)
                cell.btnMinus.addTarget(self, action: #selector(self.btnSubPressed(_:)), for: .touchUpInside)
                
                if(self.productListModel?[indexPath.row].isAdded == true){
                    cell.btnAdd.isHidden = true
                }else{
                    cell.btnAdd.isHidden = false
                }
                
                return cell
            }
        }else if tableView == self.tblProductVariant{
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.productVariantCell, for: indexPath) as? ProductVariantCell{
                
                cell.selectionStyle = .none
                
                cell.variantListData = self.variantData[indexPath.row]
                
                
                //                if(self.variantData[indexPath.row].isSelected == true){
                if(self.selectedVariantRow == indexPath.row){
                    cell.imgRadio.image = UIImage.init(named: "radio")
                }else{
                    cell.imgRadio.image = UIImage.init(named: "radiounselected")
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tblProductVariant{
            self.selectedVariantRow = indexPath.row
            self.variantData.map { varObj in
                varObj.isSelected = false
            }
            //            self.variantData[indexPath.row].isSelected = true
            
            DispatchQueue.main.async {
                self.tblProductVariant.reloadData()
                self.lblItemTotal.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: "Item Total ₹ \( self.variantData[indexPath.row].prdVarPrice ?? "0")")
            }
            
        }
    }
    
}

//MARK: - button actions
extension ProductListView{
    
    @IBAction func btnRepeatItemPressed(_ sender: UIButton){
        if(self.selectedCartData.contains(where: { cartObj in
            cartObj.productModel?.prdId ?? "" == self.selectedProduct?.prdId ?? ""
        })){
            
            self.viewPrevCustParent.isHidden = true
            self.viewPrevCustParent.isHidden = true
            
            if let prevProdObj = self.selectedCartData.last(where: { cartObj in
                cartObj.productModel?.prdId ?? "" == self.selectedProduct?.prdId ?? ""
            }){
                self.selectedCartData.append(prevProdObj)
                if let varPrice = Int(prevProdObj.variantModel?.prdVarPrice ?? "0"){
                    self.itemTotalPrice = self.itemTotalPrice + varPrice
                    
                    self.lblTotalAmtValue.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: "₹ \(self.itemTotalPrice)")
                }
                
            }
            
            
            DispatchQueue.main.async {
                self.tblProductList.reloadData()
            }
        }else{
            //MARK: - product not added previously
            
        }
    }
    
    @IBAction func btnWillChoosePressed(_ sender: UIButton){
        
        self.selectedVariantRow = nil
        
        DispatchQueue.main.async {
            self.tblProductVariant.reloadData()
        }
        
        self.viewPrevCustParent.isHidden = true
        self.viewPrevCustParent.isHidden = true
        
        self.viewParentVariant.isHidden = false
        self.viewProductVariant.isHidden = false
    }
    
    
    //MARK:- when add is pressed in front of the product
    @objc func btnAddPressed(_ sender: UIButton){
        
        if let productModel = self.productListModel?[sender.tag]{
            self.cartProducts?.append(productModel)
        }
        
        
        //MARK:- to add into the selected product variants
        self.selectedProduct = self.productListModel?[sender.tag]
        
        if let prodVariants = self.productListModel?[sender.tag].ptrPrdvariantList{
            self.variantData = prodVariants
        }
        
        self.productListModel?[sender.tag].isAdded = true
        DispatchQueue.main.async {
            self.tblProductList.reloadData()
        }
        
    }
    
    
    //MARK:- when plus is pressed in front of the product
    @objc func btnPlusPressed(_ sender: UIButton){
        
        self.selectedProduct = self.productListModel?[sender.tag]
        
        if(self.selectedCartData.contains(where: { cartObj in
            cartObj.productModel == self.productListModel?[sender.tag]
        })){
            self.viewPrevCustParent.isHidden = false
            self.viewPrevCustChild.isHidden = false
            
            let prevProdObj = self.selectedCartData.last { cartObj in
                cartObj.productModel == self.productListModel?[sender.tag]
            }
            
            self.lblPrevCustTitle.backgroundColor = UIColor.init(hexString: ColorCodes.lightGreyColor)
            
            self.lblPrevCustTitle.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 18, text: "Your previous customization")
            
            self.lblPrevCustItemTitle.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: prevProdObj?.productModel?.prdName ?? "")
            
            self.lblPrevCustItemVariantTitle.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: prevProdObj?.variantModel?.prdVarName ?? "")
            
        }else{
            if let selectedVariantRow = self.selectedVariantRow{
                self.selectedCartData.append(CartModel.init(productModel: self.selectedProduct, variantModel: self.variantData[selectedVariantRow], quantity: 1))
            }
        }
        
        DispatchQueue.main.async {
            self.tblProductList.reloadData()
        }
    }
    
    
    //MARK:- when sub is pressed in front of the product
    @objc func btnSubPressed(_ sender: UIButton){
        
        self.selectedProduct = self.productListModel?[sender.tag]
        
        if(self.selectedCartData.contains(where: { cartObj in
            cartObj.productModel?.prdId ?? "" == self.selectedProduct?.prdId ?? ""
        })){
            self.selectedCartData.removeLast()
        }
        
        
        
        DispatchQueue.main.async {
            self.tblProductList.reloadData()
        }
    }
    
    
    //MARK:- when add variant is pressed in the bottom variant view
    @IBAction func btnAddVariantPressed(_ sender: UIButton){
        
        if let selectedVariantRow = self.selectedVariantRow{
            self.variantData[selectedVariantRow].isSelected = true
            
            self.selectedCartData.append(CartModel.init(productModel: self.selectedProduct, variantModel: self.variantData[selectedVariantRow], quantity: 1))
            
            
            self.viewParentVariant.isHidden = true
            self.viewProductVariant.isHidden = true
            self.selectedVariantRow = nil
        }else{
            super.showAlertViewWithTitle("Error", message: "Please select a variant.")
        }
    }
    
    //MARK:- when proceed button is pressed navigate to the my cart screen
    @IBAction func btnProceedPressed(_ sender: UIButton){
        if let objMyCartVC = Storyboard.main.instantiateViewController(withIdentifier: "MyCartView") as? MyCartView {
            objMyCartVC.cartProducts = self.selectedCartData
            objMyCartVC.productListModel = self.productListModel?.filter({ prodDetailObj in
                prodDetailObj.isAdded == true
            })
            self.navigationController?.pushViewController(objMyCartVC, animated: true)
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            if touch.view == self.viewParentVariant{
                self.viewParentVariant.isHidden = true
                self.viewProductVariant.isHidden = true
                self.selectedVariantRow = nil
                self.selectedProduct = nil
            }
            else if touch.view == self.viewPrevCustParent{
                self.viewPrevCustParent.isHidden = true
                self.viewPrevCustParent.isHidden = true
            }
        }
    }
}

//MARK: - API calls and methods
extension ProductListView: ProductListViewModelDelegate{
    
    
    func didFetchProductListData(responseData: Any) {
        self.productListViewModel.parseProductListData(completion: {[weak self] isSuccess, message, title, dataObj in
            //self?.stopSpinner()
            if(isSuccess){
                self?.productListModel?.append(contentsOf: dataObj?.data ?? [ProductDataModel]())
            }else{
                self?.showAlertViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    func didFailFetchProductListData(error: NSError) {
        super.showAlertViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
    
    
}


//MARK: - animate bottom view for total amount and proceed button
extension ProductListView{
    
}
