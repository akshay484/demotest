//
//  ProductVariantCell.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 25/09/21.
//

import UIKit

class ProductVariantCell: UITableViewCell {
    
    
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblVariant: UILabel!
    
    var variantListData: ProductVariantListData?{
        didSet{
            if let variantListData = self.variantListData{
            self.lblVariant.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: variantListData.prdVarName ?? "")
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
