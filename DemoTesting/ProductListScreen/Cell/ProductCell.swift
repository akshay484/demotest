//
//  ProductCell.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 24/09/21.
//

import UIKit
import SDWebImage

class ProductCell: UITableViewCell {
    
    
    @IBOutlet weak var lblProdName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscPrice: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var lblDivider: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var stackButtons: UIStackView!
    @IBOutlet weak var viewStackButtons: UIView!
    
    
    var productDataModel: ProductDataModel?{
        didSet{
            if let productDataModel = productDataModel{
                self.lblProdName.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textBlackColor, fontSize: 15, text: productDataModel.prdName ?? "")
                
                self.lblCategory.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textGreyColor, fontSize: 13, text: productDataModel.prdBrandName ?? "")
                
                self.lblRating.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textGreyColor, fontSize: 13, text: "Rating \(productDataModel.prdRating ?? "")")
                
                self.lblQty.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.whiteColor, fontSize: 15, text: "\(productDataModel.qty)")
                
                self.lblPrice.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.redColor, fontSize: 13, text: "₹ \(productDataModel.prdPrice ?? "")")
                
                self.lblDivider.backgroundColor = UIColor.init(hexString: ColorCodes.dividerColor)
                
                if let prdPriceShow = productDataModel.prdPriceShow{
                    if prdPriceShow != ""{
                        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "₹ \(prdPriceShow)")
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                        self.lblDiscPrice.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textGreyColor, fontSize: 13, text: "")
                        self.lblDiscPrice.attributedText = attributeString
                    }
                }
                
                
                self.imgProduct.sd_setImage(with: URL.init(string: productDataModel.prdImageUrl ?? ""), completed: nil)
                
                
                
                self.btnAdd.setTitle("ADD", for: .normal)
                self.btnPlus.setTitle("+", for: .normal)
                self.btnMinus.setTitle("-", for: .normal)
                
                self.btnAdd.setTitleColor(UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
                self.btnPlus.setTitleColor(UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
                self.btnMinus.setTitleColor(UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
                
                self.viewStackButtons.layer.cornerRadius = 8
                self.btnAdd.layer.cornerRadius = 8
                self.btnPlus.layer.cornerRadius = 8
                self.btnMinus.layer.cornerRadius = 8
                
                self.btnAdd.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
                self.btnPlus.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
                self.btnMinus.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
                self.stackButtons.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
