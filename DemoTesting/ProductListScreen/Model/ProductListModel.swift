//
//  ProductListModel.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 24/09/21.
//

import Foundation
import ObjectMapper

class ProductListModel: NSObject, Mappable {
    var isSuccess : Bool?
    var message : String?
    var data : [ProductDataModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        isSuccess <- map["IsSuccess"]
        message <- map["Message"]
        data <- map["Data"]
        
    }
    
}


class ProductDataModel: NSObject, Mappable {
    var catName : String?
    var prdId : String?
    var prdName : String?
    var prdPriceShow : String?
    var prdPrice: String?
    var prdImageUrl: String?
    var subCatName: String?
    var prdBrandName: String?
    var prdRating: String?
    var ptrPrdvariantList: [ProductVariantListData]?
    var isAdded: Bool = false
    var qty = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        catName <- map["CatName"]
        prdId <- map["PrdId"]
        prdName <- map["PrdName"]
        prdPriceShow <- map["PrdPriceShow"]
        prdPrice <- map["PrdPrice"]
        subCatName <- map["SubCatName"]
        prdImageUrl <- map["PrdImageUrl"]
        prdBrandName <- map["PrdBrandName"]
        prdRating <- map["PrdRating"]
        ptrPrdvariantList <- map["PtrPrdvariantList"]
    }
    
}


class ProductVariantListData: NSObject, Mappable {
    var prdVarId : String?
    var prdVarPrice : String?
    var prdVarName : String?
    var remainingQuantity : String?
    var isSelected: Bool = false
    var selectedQty: [String: Int] = [:]
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        prdVarId <- map["PrdVarId"]
        prdVarPrice <- map["PrdVarPrice"]
        prdVarName <- map["PrdVarName"]
        remainingQuantity <- map["RemainingQuantity"]
    }
    
}
