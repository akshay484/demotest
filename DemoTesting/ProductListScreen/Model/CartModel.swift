//
//  CartModel.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 26/09/21.
//

import Foundation


class CartModel{
    var productModel: ProductDataModel?
    var variantModel: ProductVariantListData?
    var quantity: Int?
    
    required init(productModel: ProductDataModel?,variantModel: ProductVariantListData?,quantity: Int?){
        self.productModel = productModel
        self.variantModel = variantModel
        self.quantity = quantity
    }
}
