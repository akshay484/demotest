//
//  MyCartFooterCell.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 26/09/21.
//

import UIKit

class MyCartFooterCell: UITableViewCell {
    
    
    @IBOutlet weak var lblOrderTotal: UILabel!
    @IBOutlet weak var lblOrderTotalValue: UILabel!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var viewTerms: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        self.lblOrderTotal.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textBlackColor, fontSize: 18, text: "Order Total")
        
        self.lblOrderTotalValue.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textGreyColor, fontSize: 18, text: "")
        
        self.lblTerms.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textGreyColor, fontSize: 13, text: "Terms")
        
        self.btnProceed.setTitle("PROCEED", for: .normal)
        self.btnProceed.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.btnProceed.setTitleColor(UIColor.init(hexString: ColorCodes.whiteColor), for: .normal)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
