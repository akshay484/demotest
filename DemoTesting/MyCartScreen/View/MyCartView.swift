//
//  MyCartView.swift
//  DemoTesting
//
//  Created by Aditya_mac_4 on 26/09/21.
//

import UIKit

class MyCartView: UIViewController {
    
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var lblNavTitle: UILabel!
    @IBOutlet weak var tblCartData: UITableView!
    var totalPrice: Float?
    var cartProducts: [CartModel]?{
        didSet{
            if let cartProducts = self.cartProducts{
                for objProd in cartProducts{
                    let dictKey = "\(objProd.productModel?.prdId ?? "")_\(objProd.variantModel?.prdVarId ?? "")"
                    self.cartProductsDict.append(element: objProd, toValueOfKey: dictKey)
                    let individualPrice = Float(objProd.variantModel?.prdVarPrice ?? "0") ?? 0.0
                    self.totalPrice = individualPrice + (self.totalPrice ?? 0.0)
                }
            }
        }
    }
    
    var cartProductsDict: [String: [CartModel]] = [String: [CartModel]]()
    
    var productListModel: [ProductDataModel]?
    
    var isTermsViewHidden = true{
        didSet{
            DispatchQueue.main.async {
                self.tblCartData.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblCartData.delegate = self
        self.tblCartData.dataSource = self
        self.setupUISettings()
        
        
        DispatchQueue.main.async {
            self.tblCartData.reloadData()
        }
    }
    
    
    
    //MARK: - UI settings
    func setupUISettings(){
        self.navView.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.lblNavTitle.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.whiteColor, fontSize: 18, text: "Cart")
    }
    
    //MARK: - back navigation action
    @IBAction func btnNavigateBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}


extension MyCartView: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.cartProductsDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.myCartCell, for: indexPath) as? MyCartCell{
            
            let prodDictObj = self.cartProductsDict[indexPath.row]
            
            let variantKey = prodDictObj.key.split(separator: "_", maxSplits: 1, omittingEmptySubsequences: false)[1]
            
            let variantObj = prodDictObj.value.first?.productModel?.ptrPrdvariantList?.first(where: { prodVariantObj in
                (prodVariantObj.prdVarId ?? "") == variantKey
            })
            
            let prdPrice = Float(variantObj?.prdVarPrice ?? "0.0") ?? 0.0
            
            let totalItemPrice = Float(prodDictObj.value.count) * prdPrice
            
            
            
            cell.lblQty.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 12, text: "\(prodDictObj.value.count)")
            
            cell.imgProduct.sd_setImage(with: URL.init(string: prodDictObj.value.first?.productModel?.prdImageUrl  ?? ""), completed: nil)
            
            cell.lblProductName.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 12, text:  prodDictObj.value.first?.productModel?.prdName ?? "")
            
            cell.lblProductVariant.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 12, text:  "₹ \(prdPrice) / \(variantObj?.prdVarName ?? "")")
            
            cell.lblProductPrice.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 11, text:  "₹ \(totalItemPrice)")
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if let footerCell = tableView.dequeueReusableCell(withIdentifier: "MyCartFooterCell") as? MyCartFooterCell{
            
            footerCell.lblOrderTotal.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 14, text: "Order Total")
            
            footerCell.lblOrderTotalValue.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textBlackColor, fontSize: 14, text: "₹ \(self.totalPrice ?? 0)")
            
            footerCell.lblTerms.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textBlackColor, fontSize: 12, text: "Terms")
            
            footerCell.btnProceed.layer.cornerRadius = 8
            
            footerCell.btnTerms.addTarget(self, action: #selector(self.btnTapTerms), for: .touchUpInside)
            
            if (self.isTermsViewHidden == true){
                footerCell.viewTerms.isHidden = true
            }else{
                footerCell.viewTerms.isHidden = false
            }
            
            
            return footerCell.contentView
        }
        
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (self.isTermsViewHidden == true){
            return 150
        }else{
            return 250
        }
       
    }
    
    
    @objc func btnTapTerms(_ sender: UIButton){
        print("btnTapTerms")
        if (self.isTermsViewHidden == true){
            self.isTermsViewHidden = false
        }else{
            self.isTermsViewHidden = true
        }
    }
}


//MARK: - Dictionary extension
extension Dictionary where Value: RangeReplaceableCollection {
    public mutating func append(element: Value.Iterator.Element, toValueOfKey key: Key) -> Value? {
        var value: Value = self[key] ?? Value()
        value.append(element)
        self[key] = value
        return value
    }
    
    
    
    subscript(i: Int) -> (key: Key, value: Value){
        get{
            return self[index(startIndex,offsetBy: i)]
        }
    }
}


//MARK: - array extension
extension Array {
    func grouped<T>(by criteria: (Element) -> T) -> [T: [Element]] {
        var groups = [T: [Element]]()
        for element in self {
            let key = criteria(element)
            if groups.keys.contains(key) == false {
                groups[key] = [Element]()
            }
            groups[key]?.append(element)
        }
        return groups
    }
}



//MARK: - sequence extension
extension Sequence {
    
    func groupBy<G: Hashable>(closure: (Iterator.Element)->G) -> [G: [Iterator.Element]] {
        var results = [G: Array<Iterator.Element>]()
        
        forEach {
            let key = closure($0)
            
            if var array = results[key] {
                array.append($0)
                results[key] = array
            }
            else {
                results[key] = [$0]
            }
        }
        
        return results
    }
}

