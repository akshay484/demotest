//
//  AppMainVC.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import Foundation
import UIKit


class AppMainVc: UIViewController{

       //MARK:- Aleart View
       func showAlertViewWithTitle(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
           self.present(alert, animated: true, completion: nil)
       }
       
       func showAlertWithMessage(_ message: String, with completion: (() -> Void)?){
           let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
           self.present(alertController, animated: true, completion: nil)
           let delay = 3.0 * Double(NSEC_PER_SEC)
           let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
           DispatchQueue.main.asyncAfter(deadline: time, execute: {
               alertController.dismiss(animated: true, completion: nil)
               completion?()
           })
       }
    
}
