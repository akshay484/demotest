//
//  Constants.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire

//MARK: -  Storyboards

struct Storyboard {
    static var launchScreen: UIStoryboard {
        return UIStoryboard(name: "LaunchScreen", bundle: Bundle.main)
    }
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}

//MARK: - API Endpoints, Base URL & API Keys
struct API{
    static let productListURL = "http://www.deshizon.com/api/Fetch_ProductList_By_PartnerId_SubCatId?SubCatId=19&PartnerId=21"
}

//MARK: - error message values
struct ErrorMessages{
    static let APIError = "Something went bad!"
    static let APIAlertTitle = "Alert"
    static let noInternetTitle = "No Internet Connection"
    static let noInternetMessage = "Please check your internet connection."
}

//MARK: - font values
struct Fonts{
    static let openSansSemibold =  "OpenSans-SemiBold"
    static let openSansRegular =  "OpenSans-Regular"
}


//MARK: - color code values
struct ColorCodes{
    static let textBlackColor =  "#000000"
    static let textGreyColor =  "#9d9d9d"
    static let mainThemeColor =  "#0091aa"
    static let whiteColor =  "#ffffff"
    static let redColor =  "#ff0000"
    static let dividerColor =  "#e4e4e4"
    static let darkGreyColor =  "#999899"
    static let lightGreyColor =  "#dadada"
}



//MARK: - corner radius values
struct CornerRadius{
    static let standard = 16
}


//MARK:- cell identifiers for table views
struct CellIdentifiers{
    static let productCell = "ProductCell"
    static let productVariantCell = "ProductVariantCell"
    static let myCartCell = "MyCartCell"
}


/* Create request header */
func getHeader() -> HTTPHeaders {
    var header = HTTPHeaders()
    header["Accept"] = "application/json"
    header["Content-Type"] = "application/json"
    return header
}

